package GUItoData;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Main extends JFrame {
    private JTextField namaBarangField, namaPenitipField, lamaPenitipanField;
    private JButton submitButton;
    private JPanel resultPanel;
    private List<JLabel> namaBarangLabels, namaPenitipLabels, lamaPenitipanLabels;
    private Connection connection;

    public Main() {
        setTitle("Penitipan Barang");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 400);
        setLocationRelativeTo(null);

        JPanel mainPanel = new JPanel(new GridLayout(4, 2, 10, 10));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        mainPanel.add(new JLabel("Nama Barang"));
        namaBarangField = new JTextField();
        mainPanel.add(namaBarangField);

        mainPanel.add(new JLabel("Nama Penitip"));
        namaPenitipField = new JTextField();
        mainPanel.add(namaPenitipField);

        mainPanel.add(new JLabel("Lama Penitipan (hari)"));
        lamaPenitipanField = new JTextField();
        mainPanel.add(lamaPenitipanField);

        submitButton = new JButton("Submit");
        submitButton.addActionListener(new SubmitButtonListener());
        mainPanel.add(submitButton);

        resultPanel = new JPanel(new GridLayout(0, 3, 10, 10));
        namaBarangLabels = new ArrayList<>();
        namaPenitipLabels = new ArrayList<>();
        lamaPenitipanLabels = new ArrayList<>();

        add(mainPanel, "North");
        add(resultPanel, "Center");

        connectToDatabase(); // Connect to the database when the application starts
    }

    private class SubmitButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String namaBarang = namaBarangField.getText();
            String namaPenitip = namaPenitipField.getText();
            int lamaPenitipan = Integer.parseInt(lamaPenitipanField.getText());

            JLabel namaBarangLabel = new JLabel(namaBarang);
            JLabel namaPenitipLabel = new JLabel(namaPenitip);
            JLabel lamaPenitipanLabel = new JLabel(String.valueOf(lamaPenitipan));

            namaBarangLabels.add(namaBarangLabel);
            namaPenitipLabels.add(namaPenitipLabel);
            lamaPenitipanLabels.add(lamaPenitipanLabel);

            resultPanel.add(namaBarangLabel);
            resultPanel.add(namaPenitipLabel);
            resultPanel.add(lamaPenitipanLabel);

            revalidate();
            repaint();

            // Save the data to the database
            saveDataToDatabase(namaBarang, namaPenitip, lamaPenitipan);
        }
    }

    public void connectToDatabase() {
        try {
            String url = "jdbc:mysql://localhost:3306/penitipan";
            String user = "root";
            String password = "1234";
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the database successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveDataToDatabase(String namaBarang, String namaPenitip, int lamaPenitipan) {
        String query = "INSERT INTO datauser (NAMA_BARANG, NAMA, LAMA_PENITIPAN) VALUES (?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, namaBarang);
            preparedStatement.setString(2, namaPenitip);
            preparedStatement.setInt(3, lamaPenitipan);
            preparedStatement.executeUpdate();
            System.out.println("Data saved successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
}
